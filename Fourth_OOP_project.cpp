﻿#include <iostream>
#include <string>

/*

Задача 1.

Создайте класс Документ
-название
-тип документа (enum)

и класс Принтер
-название устройства

необходимо реализовать функцию "Печатать" у принтера. (какой принтер, что печатает)
При этом в параметры необходимо передавать ссылку на объект печатаемого документа

и функцию "проверка документа перед печатью"
Усли документ типа zip, то печать не происходит.

обе фунции должны быть дружественными для класса "Принтер".

Реализуйте задачу, используя механизм дружественных функций
*/

enum class DocumentType {
    PDF,
    TXT,
    ZIP
};

class Document;

class Printer {
private:
    std::string deviceName;

public:
    Printer(const std::string& name) : deviceName(name) {}

    friend void printDocument(Printer& printer, const Document& document);

    friend void checkDocument(Printer& printer, const Document& document);
};

class Document {
private:
    std::string title;
    DocumentType type;

public:
    Document(const std::string& t, DocumentType docType) : title(t), type(docType) {}

    friend void printDocument(Printer& printer, const Document& document);

    friend void checkDocument(Printer& printer, const Document& document);
};

void printDocument(Printer& printer, const Document& document) {
    std::cout << "Printing document '" << document.title << "' on printer '" << printer.deviceName << "'\n\n\n";
}

void checkDocument(Printer& printer, const Document& document) {
    if (document.type == DocumentType::ZIP) {
        std::cout << "Cannot print ZIP document '" << document.title << "' on printer '" << printer.deviceName << "'\n\n";
    }
    else {
        printDocument(printer, document);
    }
}

int main() {
    Printer printer("Printer1");

    Document doc1("Document1", DocumentType::PDF);
    checkDocument(printer, doc1);

    Document doc2("Document2", DocumentType::ZIP);
    checkDocument(printer, doc2);

    return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*

Задача 2

Создать примитивный шаблонный класс "Динамический массив"

Нужно реализовать механизм создания массива (динамически)
Методы (добавления и удаление элементов)

При удалении элементов, мы не затираем элемент, а перераспределяем память
т.е. создаём новый массив на 1 элемент меньше, копируем туда старый, а старый удаляем

Доп:
можно реализовать перегрузки методов удаления и добавления по индексу.

*/


/*template<typename T>
class DinamicArray {
private:
    T* data;
    int size;
public:
    DinamicArray(): data(nullptr), size(0){}

    void addElement(const T& element) {
        T* newData = new T[size + 1];
        for (int i = 0; i < size; i++)
        {
            newData[i] = data[i];
        }
        newData[size] = element;
        delete[] data;
        data = newData;
        size++;
    }

    void removeLastElement() {
        if (size > 0)
        {
            T* newData = new T[size - 1];
            for (int i = 0; i < size - 1; i++)
            {
                newData[i] = data[i];
            }
            delete[] data;
            data = newData;
            size--;
        }
    }

    void addElementAtIndex(const T& element, int index) {
        if (index < 0 || index > size)
        {
            std::cout << "Index out of bounds.\n";
            return;
        }

        T* newData = new T[size + 1];
        int j = 0;
        for (int i = 0; i < size + 1; i++)
        {
            if (i == index)
            {
                newData[i] = element;
            }
            else
            {
                newData[i] = data[j];
                j++;
            }
        }
        delete[] data;
        data = newData;
        size++;
    }

    void removeElementAtIndex(int index) {
        if (index < 0 || index >= size)
        {
            std::cout << "Index out of bounds.\n";
            return;
        }

        T* newData = new T[size - 1];
        int j = 0;
        for (int i = 0; i < size; i++)
        {
            if (i != index)
            {
                newData[j] = data[i];
                j++;
            }
        }
        delete[] data;
        data = newData;
        size--;
    }

    void printArray() const {
        for (int i = 0; i < size; i++)
        {
            std::cout << data[i] << " ";
        }
        std::cout << "\n";
    }

    ~DinamicArray() {
        delete[] data;
    }
};

int main()
{
    DinamicArray<int> arr;
    arr.addElement(1);
    arr.addElement(2);
    arr.addElement(3);
    arr.printArray();

    arr.removeLastElement();
    arr.printArray();

    arr.addElementAtIndex(4, 2);
    arr.printArray();

    arr.removeElementAtIndex(2);
    arr.printArray();

    return 0;
}
*/